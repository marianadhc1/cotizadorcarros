const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");

//declarar arrays de objeto
let datos = [{
    matricula: "2020030323",
    nombre: "HERNANDEZ COLIO MARIANA DE JESUS",
    sexo: 'F',
    materias: ["Ingles", "Tecnologías de Internet", "Base de datos"]
},
{
    matricula: "2020030350",
    nombre: "SOTO OSUNA ALONDRA GUADALUPE",
    sexo: 'F',
    materias: ["Ingles", "Tecnologías de Internet", "Base de datos"]
},
{
    matricula: "2020030360",
    nombre: "PADILLA MORENO LIZBET GUADALUPE",
    sexo: 'F',
    materias: ["Ingles", "Tecnologías de Internet", "Base de datos"]
},
{
    matricula: "2020030365",
    nombre: "PUGA VAZQUEZ ISSAC DAVID",
    sexo: 'M',
    materias: ["Ingles", "Tecnologías de Internet", "Base de datos"]
},
{
    matricula: "2020030380",
    nombre: "HERNANDEZ IBARGUEN JUSTIN ADILAN",
    sexo: 'M',
    materias: ["Ingles", "Tecnologías de Internet", "Base de datos"]

}]

router.get('/', (req,res)=>{
    //res.send("<h1> Hola! Iniciamos express </h1>");
    res.render('index.html', {titulo:"Listado de Alumnos", listado:datos})
})

router.get("/tablas", (req, res)=>{
    const valores = {
        tabla:req.query.tabla
    }

    res.render('tablas.html', valores);
})

router.post("/tablas", (req, res)=>{
    const valores = {
        tabla:req.body.tabla
    }

    res.render('tablas.html', valores);
})

router.get("/cotizacion", (req, res)=>{
    const valores = {
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.query.plazos, 
    }
    res.render('cotizacion.html', valores);
})

router.post("/cotizacion", (req, res)=>{
    const valores = {
        valor:req.body.valor,
        pInicial:req.body.pInicial,
        plazos:req.body.plazos, 
    }
    res.render('cotizacion.html', valores);
})

module.exports = router; 